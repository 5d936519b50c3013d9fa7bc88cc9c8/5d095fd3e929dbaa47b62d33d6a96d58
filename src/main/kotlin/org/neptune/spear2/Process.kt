package org.neptune.spear2

interface Process : Source {

	val id: Int

	val modules: Map<String, Module>

	fun loadModules()

}
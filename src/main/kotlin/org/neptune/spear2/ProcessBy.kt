@file:JvmName("ProcessBy")

package org.neptune.spear2

import com.sun.jna.Platform
import com.sun.jna.platform.win32.WinNT
import org.neptune.spear2.windows.Windows

@JvmOverloads
fun processByID(processID: Int, accessFlags: Int = WinNT.PROCESS_ALL_ACCESS): Process? = when {
	Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processID, accessFlags)
	else -> null
}

@JvmOverloads
fun processByName(processName: String, accessFlags: Int = WinNT.PROCESS_ALL_ACCESS): Process? = when {
	Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processName, accessFlags)
	else -> null
}